#include <stdio.h>
int main()
{
    int n, i, c= 0;
    printf("Enter a number: ");
    scanf("%d", &n);
    for(i = 2; i <= n/2; ++i)
    {
        if(n%i == 0)
        {
            c = 1;
            break;
        }
    }
    if (n == 0) 
    {
      printf("0 is neither a prime nor a composite number.");
    }
    else 
    {
        if (c== 0)
          printf("%d is a prime number.", n);
        else
          printf("%d is a composite number.", n);
    }
    
    return 0;
}