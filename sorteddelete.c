#include<stdio.h>
int main()
{
    int i,j,n,a[10],num;
    printf("Enter the number of elements of the array\n");
    scanf("%d",&n);
    printf("Enter the elements of the array: ");
    for(i=0; i<n; i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the number to be deleted\n");
    scanf("%d",&num);
    for(i=0; i<n; i++)
    {
        if(a[i]==num)
        {
            for(j=i; j<n-1; j++)
            {
                a[j]=a[j+1];
            }
        }
    }
    n=n-1;
    printf("The array after deleting the number is :\n");
    for(i=0; i<n; i++)
    {
        printf("%d",a[i]);
    }
    return 0;
}

